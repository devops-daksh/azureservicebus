package com.test.receiver;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import com.azure.messaging.servicebus.ServiceBusClientBuilder;
import com.azure.messaging.servicebus.ServiceBusReceiverAsyncClient;

public class TopicReceiverPeekMassage {
	static String connectionString = "Endpoint=sb://manjeshservicebus.servicebus.windows.net/;SharedAccessKeyName=testtest;SharedAccessKey=J18hKycK+spAllqriNNgmv/4X5zotyy1OFNwrB5RYZ8=;EntityPath=mytopic";
	static String subscriptionName = "mysubscription";    
	static String topicName = "mytopic";

    /**
     * Main method to invoke this demo on how to peek at a message within a Service Bus Queue.
     *
     * @param args Unused arguments to the program.
     * @throws InterruptedException If the program is unable to sleep while waiting for the receive to complete.
     */
    public static void main(String[] args) throws InterruptedException {
    	TopicReceiverPeekMassage sample = new TopicReceiverPeekMassage();
        sample.run();
    }

    /**
     * run method to invoke this demo on how to peek at a message within a Service Bus Queue.
     *
     * @throws InterruptedException If the program is unable to sleep while waiting for the receive to complete.
     */
    
    public void run() throws InterruptedException {
        AtomicBoolean sampleSuccessful = new AtomicBoolean(false);
        CountDownLatch countdownLatch = new CountDownLatch(1);

        // The connection string value can be obtained by:
        // 1. Going to your Service Bus namespace in Azure Portal.
        // 2. Go to "Shared access policies"
        // 3. Copy the connection string for the "RootManageSharedAccessKey" policy.
        // The 'connectionString' format is shown below.
        // 1. "Endpoint={fully-qualified-namespace};SharedAccessKeyName={policy-name};SharedAccessKey={key}"
        // 2. "<<fully-qualified-namespace>>" will look similar to "{your-namespace}.servicebus.windows.net"
        // 3. "queueName" will be the name of the Service Bus queue instance you created
        //    inside the Service Bus namespace.

        // Create a receiver using connection string.
        ServiceBusReceiverAsyncClient receiver = new ServiceBusClientBuilder()
            .connectionString(connectionString)
            .receiver()
            .topicName(topicName)
            .subscriptionName(subscriptionName)
            .buildAsyncClient();

        receiver.peekMessage().subscribe(
            message -> {
                System.out.println("Received Message Id: " + message.getMessageId());
                System.out.println("Received Message: " + message.getBody().toString());
            },
            error -> System.err.println("Error occurred while receiving message: " + error),
            () -> {
                System.out.println("Receiving complete.");
                sampleSuccessful.set(true);
            });

        // Subscribe is not a blocking call so we wait here so the program does not end while finishing
        // the peek operation.
        countdownLatch.await(10, TimeUnit.SECONDS);

        // Close the receiver.
        receiver.close();

        // This assertion is to ensure that samples are working. Users should remove this.
        System.out.println(sampleSuccessful.get());
    }
}
