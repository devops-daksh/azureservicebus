package com.test.receiver;

import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import com.azure.messaging.servicebus.ServiceBusClientBuilder;
import com.azure.messaging.servicebus.ServiceBusErrorContext;
import com.azure.messaging.servicebus.ServiceBusException;
import com.azure.messaging.servicebus.ServiceBusFailureReason;
import com.azure.messaging.servicebus.ServiceBusProcessorClient;
import com.azure.messaging.servicebus.ServiceBusReceivedMessageContext;
import com.azure.messaging.servicebus.ServiceBusReceiverAsyncClient;

public class TopicReceiverFirst {
	
	
	static String connectionString = "Endpoint=sb://manjeshservicebus.servicebus.windows.net/;SharedAccessKeyName=testtest;SharedAccessKey=J18hKycK+spAllqriNNgmv/4X5zotyy1OFNwrB5RYZ8=;EntityPath=mytopic";
	static String subscriptionName = "mysubscription";    
	static String topicName = "mytopic";
	
	
	// Sample code that processes a single message
	static Consumer<ServiceBusReceivedMessageContext> processMessage = messageContext -> {
	    try {
	        System.out.println(messageContext.getMessage().getMessageId());
	        // other message processing code
	        messageContext.complete();
	    } catch (Exception ex) {
	        messageContext.abandon();
	    }
	};

	// Sample code that gets called if there's an error
	static Consumer<ServiceBusErrorContext> processError = context -> {
	    System.err.println("Error occurred while receiving message: " + context.getException());
	    System.out.printf("Error when receiving messages from namespace: '%s'. Entity: '%s'%n",
	            context.getFullyQualifiedNamespace(), context.getEntityPath());

        if (!(context.getException() instanceof ServiceBusException)) {
            System.out.printf("Non-ServiceBusException occurred: %s%n", context.getException());
            return;
        }

        ServiceBusException exception = (ServiceBusException) context.getException();
        ServiceBusFailureReason reason = exception.getReason();

        if (reason == ServiceBusFailureReason.MESSAGING_ENTITY_DISABLED
            || reason == ServiceBusFailureReason.MESSAGING_ENTITY_NOT_FOUND
            || reason == ServiceBusFailureReason.UNAUTHORIZED) {
            System.out.printf("An unrecoverable error occurred. Stopping processing with reason %s: %s%n",
                reason, exception.getMessage());

        } else if (reason == ServiceBusFailureReason.MESSAGE_LOCK_LOST) {
            System.out.printf("Message lock lost for message: %s%n", context.getException());
        } else if (reason == ServiceBusFailureReason.SERVICE_BUSY) {
            try {
                // Choosing an arbitrary amount of time to wait until trying again.
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                System.err.println("Unable to sleep for period of time");
            }
        } else {
            System.out.printf("Error source %s, reason %s, message: %s%n", context.getErrorSource(),
                reason, context.getException());
        }
	};
	
	
	public static void main(String[] args) throws InterruptedException {
		ServiceBusProcessorClient receiver = new ServiceBusClientBuilder()
			    .connectionString(connectionString)
			    .processor()
			    .topicName(topicName)
			    .subscriptionName(subscriptionName)
			    .processMessage(processMessage)	
			    .processError(processError)
			    .disableAutoComplete()
                .buildProcessorClient();
		
		System.out.println("Starting the processor");
		receiver.start();

	    TimeUnit.SECONDS.sleep(10);
	    System.out.println("Stopping and closing the processor");
	    receiver.close(); 

	}
	
}
