package com.test.publisher;

import java.util.Arrays;
import java.util.List;

import com.azure.messaging.servicebus.ServiceBusClientBuilder;
import com.azure.messaging.servicebus.ServiceBusMessage;
import com.azure.messaging.servicebus.ServiceBusMessageBatch;
import com.azure.messaging.servicebus.ServiceBusSenderClient;

public class TopicPublisherBatch {

	static String connectionString = "Endpoint=sb://manjeshservicebus.servicebus.windows.net/;SharedAccessKeyName=testtest;SharedAccessKey=J18hKycK+spAllqriNNgmv/4X5zotyy1OFNwrB5RYZ8=;EntityPath=mytopic";
	static String topicName = "mytopic";
	static String subName = "mysubscription";

	static void sendMessage() {
		// create a Service Bus Sender client for the queue
		ServiceBusSenderClient senderClient = new ServiceBusClientBuilder()
				.connectionString(connectionString)
				.sender()
				.buildClient();

		// Creates an ServiceBusMessageBatch where the ServiceBus.
		ServiceBusMessageBatch messageBatch = senderClient.createMessageBatch();

		// create a list of messages
	    List<ServiceBusMessage> listOfMessages = createMessages();
	 // We try to add as many messages as a batch can fit based on the maximum size and send to Service Bus when
	    // the batch can hold no more messages. Create a new batch for next set of messages and repeat until all
	    // messages are sent.        
	    for (ServiceBusMessage message : listOfMessages) {
	        if (messageBatch.tryAddMessage(message)) {
	            continue;
	        }

	        // The batch is full, so we create a new batch and send the batch.
	        senderClient.sendMessages(messageBatch);
	        System.out.println("Sent a batch of messages to the topic: " + topicName);

	        // create a new batch
	        messageBatch = senderClient.createMessageBatch();

	        // Add that message that we couldn't before.
	        if (!messageBatch.tryAddMessage(message)) {
	            System.err.printf("Message is too large for an empty batch. Skipping. Max size: %s.", messageBatch.getMaxSizeInBytes());
	        }
	    }

	    if (messageBatch.getCount() > 0) {
	        senderClient.sendMessages(messageBatch);
	        System.out.println("Sent a batch of messages to the topic: " + topicName);
	    }

		senderClient.close();
	}

	public static List<ServiceBusMessage> createMessages() {
		List<ServiceBusMessage> messages = Arrays.asList(
				new ServiceBusMessage("Hello world").setMessageId("1"),
				new ServiceBusMessage("Manjesh Rathore").setMessageId("2"));

		return messages;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("testing Rathore");
		sendMessage();
		System.out.println("testing Manjesh");
	}

}
